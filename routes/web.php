<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('install', function () {
    Artisan::call('migrate:fresh');
    Artisan::call('storage:link');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');


Route::get('/material/{material}', function (\App\Material $material) {
    return view('materials.show',compact('material'));
})->name('showmaterial');


Route::group(['prefix' => 'admins','middleware'=>'auth'], function () {

    Route::get('/', function () {
        $Material = \App\Material::all();
        return view('admin.dashboard', compact('Material'));
    });

    Route::resource('addmaterial', 'MaterialController');
    Route::patch('updatematerial/{id}', 'MaterialController@update');
    Route::post('materialpost', function (\Illuminate\Http\Request $request) {
        $material = \App\Material::all()->find($request->id);
        if ($request->has('image')) {
            $imagepath = $request->file('image')->store('materials','public');
        } else {
            $imagepath = $material->image;
        }

        $material->material = $request->material;
        $material->category = $request->category;
        $material->composition = $request->composition;
        $material->appearance = $request->appearance;
        $material->structure = $request->structure;
        $material->meltingpoint = $request->meltingpoint;
        $material->boilingpoint = $request->boilingpoint;
        $material->density = $request->density;
        $material->shearmodulus = $request->shearmodulus;
        $material->youngsmodulus = $request->youngsmodulus;
        $material->bulkmodulus = $request->bulkmodulus;
        $material->image = $imagepath;
        $material->formationenergy = $request->formationenergy;
        $material->bandgapenergy = $request->bandgapenergy;
        $material->mohshardness = $request->mohshardness;
        $material->vickershardness = $request->vickershardness;
        $material->saveOrFail();

        return response()->json($material);
    });

    Route::get('/dashboard', function () {
        $Material = \App\Material::all();
        return view('admin.dashboard', compact('Material'));
    });

    Route::resource('/settings', 'SettingController');

});

Route::get('download/{id}', function ($id) {
    $material = \App\Material::find($id);

    return response()->streamDownload(function () use ($material) {
        echo
        "{
  \"material\": \"{$material->material}\",
  \"category\": \"{$material->category}\",
  \"appearance\": \"{$material->appearance}\",
  \"structure\": \"{$material->structure}\",
  \"meltingpoint\": \"{$material->meltingpoint}\",
  \"boilingpoint\": \"{$material->boilingpoint}\",
  \"density\": \"{$material->density}\",
  \"shearmodulus\": \"{$material->shearmodulus}\",
  \"youngsmodulus\": \"{$material->youngsmodulus}\",
  \"bulkmodulus\": \"{$material->bulkmodulus}\",
  \"composition\": \"{$material->composition}\"
}
";
    }, "{$material->material}.json");
});



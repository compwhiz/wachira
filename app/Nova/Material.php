<?php

namespace App\Nova;

use Laravel\Nova\Fields\File;
use Laravel\Nova\Fields\ID;
use Illuminate\Http\Request;
use Laravel\Nova\Fields\Text;
use Laravel\Nova\Http\Requests\NovaRequest;

class Material extends Resource
{
    /**
     * The model the resource corresponds to.
     *
     * @var string
     */
    public static $model = 'App\Material';

    /**
     * The single value that should be used to represent the resource when being displayed.
     *
     * @var string
     */
    public static $title = 'id';

    /**
     * The columns that should be searched.
     *
     * @var array
     */
    public static $search = [
        'material',
    ];

    /**
     * Get the fields displayed by the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function fields(Request $request)
    {
        return [
            Text::make('Material','material'),
            Text::make('Category','category'),
            Text::make('Composition','composition'),
            Text::make('Appearance','appearance'),
            Text::make('Structure','structure'),
            Text::make('Melting Point','meltingpoint')->hideFromIndex(),
            Text::make('Boiling Point','boilingpoint')->hideFromIndex(),
            Text::make('Density','density')->hideFromIndex(),
            Text::make('Shear Modulus','shearmodulus')->hideFromIndex(),
            Text::make('Youngs Modulus','youngsmodulus')->hideFromIndex(),
            Text::make('Bulk Modulus','bulkmodulus')->hideFromIndex(),
            Text::make('Formatin Energy','formationenergy')->hideFromIndex(),
            Text::make('band Gap Energy','bandgapenergy')->hideFromIndex(),
            Text::make('Mohs Hardness','mohshardness')->hideFromIndex(),
            Text::make('Vickers Hardness','vickershardness')->hideFromIndex(),
            File::make('Image','image')->hideFromIndex(),
        ];
    }

    /**
     * Get the cards available for the request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function cards(Request $request)
    {
        return [];
    }

    /**
     * Get the filters available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function filters(Request $request)
    {
        return [];
    }

    /**
     * Get the lenses available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function lenses(Request $request)
    {
        return [];
    }

    /**
     * Get the actions available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function actions(Request $request)
    {
        return [];
    }
}

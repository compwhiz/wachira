<?php

namespace App\Http\Controllers;

use App\Material;
use Illuminate\Http\Request;

class MaterialController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $Material = \App\Material::all();
        return view('materials.index', compact('Material'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('materials.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     * @throws \Throwable
     */
    public function store(Request $request, Material $material)
    {
        $this->validate($request, [
            'material' => ['required'],
        ]);
        if ($request->has('image')) {
            $imagepath = $request->file('image')->store('materials','public');
        } else {
            $imagepath = '';
        }

        $material->material = $request->material;
        $material->category = $request->category;
        $material->composition = $request->composition;
        $material->appearance = $request->appearance;
        $material->structure = $request->structure;
        $material->meltingpoint = $request->meltingpoint;
        $material->boilingpoint = $request->boilingpoint;
        $material->density = $request->density;
        $material->shearmodulus = $request->shearmodulus;
        $material->youngsmodulus = $request->youngsmodulus;
        $material->bulkmodulus = $request->bulkmodulus;
        $material->image = $imagepath;
        $material->formationenergy = $request->formationenergy;
        $material->bandgapenergy = $request->bandgapenergy;
        $material->mohshardness = $request->mohshardness;
        $material->vickershardness = $request->vickershardness;
        $material->saveOrFail();

        return response()->json($material);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Material $material
     * @return \Illuminate\Http\Response
     */
    public function show(Material $material)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Material $material
     * @return \Illuminate\Http\Response
     */
    public function edit(Material $material)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\Material $material
     * @return \Illuminate\Http\Response
     * @throws \Throwable
     */
    public function update($material,Request $request)
    {


        $material = Material::all()->find($material);
        if ($request->has('image')) {
            $imagepath = $request->file('image')->store('public/materials');
        } else {
            $imagepath = $material->image;
        }

        $material->material = $request->material;
        $material->category = $request->category;
        $material->composition = $request->composition;
        $material->appearance = $request->appearance;
        $material->structure = $request->structure;
        $material->meltingpoint = $request->meltingpoint;
        $material->boilingpoint = $request->boilingpoint;
        $material->density = $request->density;
        $material->shearmodulus = $request->shearmodulus;
        $material->youngsmodulus = $request->youngsmodulus;
        $material->bulkmodulus = $request->bulkmodulus;
        $material->image = $imagepath;
        $material->formationenergy = $request->formationenergy;
        $material->bandgapenergy = $request->bandgapenergy;
        $material->mohshardness = $request->mohshardness;
        $material->vickershardness = $request->vickershardness;
        $material->saveOrFail();

        return response()->json($material);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Material $material
     * @return \Illuminate\Http\Response
     */
    public function destroy($material)
    {
        Material::all()->find($material)->delete();
        return response()->json($material);
    }
}

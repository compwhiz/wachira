<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Kenya Mineral Database</title>

    <!-- All plugins -->
    <link href="{{ asset('assets/plugins/css/plugins.css') }}" rel="stylesheet">

    <!-- Custom style -->
    <link href="{{ asset('assets/css/style.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/css/responsiveness.css') }}" rel="stylesheet">
    <link type="text/css" rel="stylesheet" id="jssDefault" href="{{ asset('assets/css/colors/main.css') }}">
</head>
<body>
<div class="wrapper">
    <!-- Start Navigation -->
    <nav class="navbar navbar-default navbar-fixed navbar-transparent white bootsnav">
        <div class="container-fluid">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-menu">
                <i class="ti-align-left"></i>
            </button>

            <!-- Start Header Navigation -->
            <div class="navbar-header">
                <a class="navbar-brand" href="">
                    {{--                        <img src="{{ asset('assets/img/logo-white.png') }}" class="logo logo-display" alt="">--}}
                    {{--                        <img src="{{ asset('assets/img/logo.png') }}" class="logo logo-scrolled" alt="">--}}
                </a>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="navbar-menu">
                <ul class="nav navbar-nav navbar-center" data-in="fadeInDown" data-out="fadeOutUp">
                    <li><a href="javascript:void(0)" data-toggle="modal" data-target="#signup">Home</a></li>
                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </div>
    </nav>
    <!-- End Navigation -->
    <div class="clearfix"></div>
    <section class="detail-section" style="background:url('{{ asset('storage/'.$material->image) }}');">
        <div class="overlay" style="background-color: rgb(36, 36, 41); opacity: 0.5;"></div>
        <div class="profile-cover-content">
            <div class="container">
                <div class="cover-buttons">
                </div>
                <div class="listing-owner hidden-xs hidden-sm">
                    <div class="listing-owner-avater">
                        <img src="{{ asset('storage/'.$material->image) }}" class="img-responsive img-circle" alt="">
                    </div>
                    <div class="listing-owner-detail">
                        <h4>{{ $material->material }}</h4>
                        <span class="theme-cl">{{ $material->category }}</span>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="list-detail">
        <div class="container">
            <div class="row">
                <!-- Start: Listing Detail Wrapper -->
                <div class="col-md-12 col-sm-12">
                    <div class="detail-wrapper">
                        <div class="detail-wrapper-body">
                            <div class="listing-title-bar">
                                <h3>{{ $material->material }} <span
                                        class="mrg-l-5 category-tag">{{$material->category}}</span></h3>
                                <div>
                                    <a href="#listing-location" class="listing-address">
                                        <i class="ti-location-pin mrg-r-5"></i>

                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="detail-wrapper">
                        <div class="detail-wrapper-header">
                            <h4>Properties</h4>
                        </div>
                        <div class="detail-wrapper-body">
                            <ul class="detail-check">
                                {{--<li>IMAGE : <b>{{ $material->image }}</b></li>--}}
                                <li>MATERIAL : <b>{{ $material->material }}</b></li>
                                <li>APPEARANCE : <b>{{ $material->appearance }}</b></li>
                                <li>CATEGORY : <b>{{ $material->category }}</b></li>
                                <li>COMPOSITION : <b>{{ $material->composition }}</b></li>
                                <li>STRUCTURE : <b>{{ $material->structure }}</b></li>
                                <li>LATTICE CONSTANT : <b>{{ $material->latticeconstant }}</b></li>
                                <li>MELTING POINT : <b>{{ $material->meltingpoint }}</b></li>
                                <li>BOILING POINT : <b>{{ $material->boilingpoint }}</b></li>
                                <li>DENSITY : <b>{{ $material->density }}</b></li>
                                <li>FERMI ENERGY : <b>{{ $material->fermienergy }}</b></li>
                                <li>DEBYE TEMPERATURE : <b>{{ $material->debyetemperature }}</b></li>
                                <li>SHEAR MODULUS : <b>{{ $material->shearmodulus }}</b></li>
                                <li>YOUNGS MODULUS : <b>{{ $material->youngsmodulus }}</b></li>
                                <li>BULK MODULUS : <b>{{ $material->bulkmodulus }}</b></li>
                                <li>LOCATION : <b>{{ $material->location }}   </b></li>

                            </ul>
                        </div>
                    </div>


                </div>
                <!-- End: Listing Detail Wrapper -->


                <!-- End: Sidebar Start -->
            </div>
        </div>
    </section>

    <!-- ================ Start Footer ======================= -->
    <footer class="footer dark-bg">
        <div class="row padd-0 mrg-0">
            <div class="footer-text">
                <div class="col-md-3 col-sm-12 theme-bg">
                    <div class="footer-widget">
                        <div class="textwidget">
                            <h3 class="widgettitle widget-title">Get In Touch</h3>
                            <p>Masinde Muliro University of Science and Technology<br>
                                Kakamega <br>
                                Department of Physics</p>
                            <p><strong>Email:</strong> kmd@cthep.ke</p>
                            <p>
                                <strong>Call:</strong> <a href="tel:+254713642175">254716 205663</a>
                            </p>
                        </div>
                    </div>
                </div>
                <div class="col-md-5 col-sm-4">
                    <div class="footer-widget">
                        <h3 class="widgettitle widget-title">About Us</h3>
                        <ul class="footer-navigation">
                            <li><a href="#">Materials</a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-md-4 col-sm-6">
                    <div class="footer-widget">
                        <h3 class="widgettitle widget-title">Connect Us</h3>
                        {{--<img src="assets/img/footer-logo.png" alt="Footer logo" class="img-responsive" />--}}
                        <ul class="footer-social">
                            <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                            <li><a href="#"><i class="fa fa-google-plus"></i></a></li>
                            <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                            <li><a href="#"><i class="fa fa-instagram"></i></a></li>
                            <li><a href="#"><i class="fa fa-pinterest"></i></a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class="footer-copyright">
            <p>Copyright@ {{ date('Y') }} KMD By <a href="http://www.compwhiz.co.ke" title="Compwhiz"
                                                    target="_blank">CTHEP</a></p>
        </div>
    </footer>
    <!-- ================ End Footer Section ======================= -->

    <!-- ================== Login & Sign Up Window ================== -->
    <div class="modal fade" id="signup" tabindex="-1" role="dialog" aria-labelledby="myModalLabel2" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-body">
                    <div class="tab" role="tabpanel">
                        <!-- Nav tabs -->
                        <ul class="nav nav-tabs" role="tablist">
                            <li role="presentation" class="active"><a href="#login" role="tab" data-toggle="tab">Sign
                                    In</a></li>
                            <li role="presentation"><a href="#register" role="tab" data-toggle="tab">Sign Up</a></li>
                        </ul>
                        <!-- Tab panes -->
                        <div class="tab-content" id="myModalLabel2">
                            <div role="tabpanel" class="tab-pane fade in active" id="login">
                                <img src="assets/img/logo.png" class="img-responsive" alt=""/>
                                <div class="subscribe wow fadeInUp">
                                    <form class="form-inline" method="post">
                                        <div class="col-sm-12">
                                            <div class="form-group">
                                                <input type="email" name="email" class="form-control"
                                                       placeholder="Username" required="">
                                                <input type="password" name="password" class="form-control"
                                                       placeholder="Password" required="">
                                                <div class="center">
                                                    <button type="submit" id="login-btn"
                                                            class="btn btn-midium theme-btn btn-radius width-200"> Login
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>

                            <div role="tabpanel" class="tab-pane fade" id="register">
                                <img src="assets/img/logo.png" class="img-responsive" alt=""/>
                                <form class="form-inline" method="post">
                                    <div class="col-sm-12">
                                        <div class="form-group">
                                            <input type="text" name="email" class="form-control" placeholder="Your Name"
                                                   required="">
                                            <input type="email" name="email" class="form-control"
                                                   placeholder="Your Email" required="">
                                            <input type="email" name="email" class="form-control" placeholder="Username"
                                                   required="">
                                            <input type="password" name="password" class="form-control"
                                                   placeholder="Password" required="">
                                            <div class="center">
                                                <button type="submit" id="subscribe"
                                                        class="btn btn-midium theme-btn btn-radius width-200"> Create
                                                    Account
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- ===================== End Login & Sign Up Window =========================== -->
    <!-- Switcher -->
{{--<button class="w3-button w3-teal w3-xlarge w3-right" onclick="openRightMenu()"><i class="spin theme-cl fa fa-cog" aria-hidden="true"></i></button>--}}
{{--<div class="w3-sidebar w3-bar-block w3-card-2 w3-animate-right" style="display:none;right:0;" id="rightMenu">--}}
{{--<button onclick="closeRightMenu()" class="w3-bar-item w3-button w3-large theme-bg">Close &times;</button>--}}
{{--<div class="title-logo">--}}
{{--<img src="assets/img/logo.png" alt="" class="img-responsive">--}}
{{--<h4>Choose Your Color</h4>--}}
{{--</div>--}}
{{--<ul id="styleOptions" title="switch styling">--}}
{{--<li>--}}
{{--<a href="javascript: void(0)" class="cl-box alabama" data-theme="colors/alabama"></a>--}}
{{--</li>--}}
{{--<li>--}}
{{--<a href="javascript: void(0)" class="cl-box imperial-blue" data-theme="colors/imperial-blue"></a>--}}
{{--</li>--}}
{{--<li>--}}
{{--<a href="javascript: void(0)" class="cl-box university" data-theme="colors/university"></a>--}}
{{--</li>--}}
{{--<li>--}}
{{--<a href="javascript: void(0)" class="cl-box cerulean" data-theme="colors/cerulean"></a>--}}
{{--</li>--}}
{{--<li>--}}
{{--<a href="javascript: void(0)" class="cl-box lilac" data-theme="colors/lilac"></a>--}}
{{--</li>--}}
{{--<li>--}}
{{--<a href="javascript: void(0)" class="cl-box bronze" data-theme="colors/bronze"></a>--}}
{{--</li>--}}
{{--<li>--}}
{{--<a href="javascript: void(0)" class="cl-box viridian-green" data-theme="colors/viridian-green"></a>--}}
{{--</li>--}}
{{--<li>--}}
{{--<a href="javascript: void(0)"  class="cl-box amaranth" data-theme="colors/amaranth"></a>--}}
{{--</li>--}}
{{--<li>--}}
{{--<a href="javascript: void(0)" class="cl-box yellow" data-theme="colors/yellow"></a>--}}
{{--</li>--}}
{{--<li>--}}
{{--<a href="javascript: void(0)" class="cl-box kelly-green" data-theme="colors/kelly-green"></a>--}}
{{--</li>--}}
{{--<li>--}}
{{--<a href="javascript: void(0)" class="cl-box lava" data-theme="colors/lava"></a>--}}
{{--</li>--}}
{{--<li>--}}
{{--<a href="javascript: void(0)" class="cl-box violet" data-theme="colors/violet"></a>--}}
{{--</li>--}}
{{--<li>--}}
{{--<a href="javascript: void(0)" class="cl-box cadmium" data-theme="colors/cadmium"></a>--}}
{{--</li>--}}
{{--<li>--}}
{{--<a href="javascript: void(0)" class="cl-box rich-carmine" data-theme="colors/rich-carmine"></a>--}}
{{--</li>--}}
{{--<li>--}}
{{--<a href="javascript: void(0)" class="cl-box ball-blue" data-theme="colors/ball-blue"></a>--}}
{{--</li>--}}
{{--<li>--}}
{{--<a href="javascript: void(0)" class="cl-box charcoal" data-theme="colors/charcoal"></a>--}}
{{--</li>--}}
{{--<li>--}}
{{--<a href="javascript: void(0)" class="cl-box lime-green" data-theme="colors/lime-green"></a>--}}
{{--</li>--}}
{{--<li>--}}
{{--<a href="javascript: void(0)" class="cl-box cg-blue" data-theme="colors/cg-blue"></a>--}}
{{--</li>--}}
{{--<li>--}}
{{--<a href="javascript: void(0)" class="cl-box crimson" data-theme="colors/crimson"></a>--}}
{{--</li>--}}
{{--<li>--}}
{{--<a href="javascript: void(0)" class="cl-box cyan-cornflower" data-theme="colors/cyan-cornflower"></a>--}}
{{--</li>--}}
{{--<li>--}}
{{--<a href="javascript: void(0)" class="cl-box dark-cyan" data-theme="colors/dark-cyan"></a>--}}
{{--</li>--}}
{{--<li>--}}
{{--<a href="javascript: void(0)" class="cl-box dark-pink" data-theme="colors/dark-pink"></a>--}}
{{--</li>--}}
{{--<li>--}}
{{--<a href="javascript: void(0)" class="cl-box debian-red" data-theme="colors/debian-red"></a>--}}
{{--</li>--}}
{{--<li>--}}
{{--<a href="javascript: void(0)" class="cl-box alizarin" data-theme="colors/alizarin"></a>--}}
{{--</li>--}}
{{--<li>--}}
{{--<a href="javascript: void(0)" class="cl-box bleu-de-france" data-theme="colors/bleu-de-france"></a>--}}
{{--</li>--}}
{{--<li>--}}
{{--<a href="javascript: void(0)" class="cl-box fuchsia" data-theme="colors/fuchsia"></a>--}}
{{--</li>--}}
{{--<li>--}}
{{--<a href="javascript: void(0)" class="cl-box forest-green" data-theme="colors/forest-green"></a>--}}
{{--</li>--}}
{{--<li>--}}
{{--<a href="javascript: void(0)" class="cl-box go-green" data-theme="colors/go-green"></a>--}}
{{--</li>--}}
{{--<li>--}}
{{--<a href="javascript: void(0)" class="cl-box azure" data-theme="colors/azure"></a>--}}
{{--</li>--}}
{{--<li>--}}
{{--<a href="javascript: void(0)" class="cl-box american-rose" data-theme="colors/american-rose"></a>--}}
{{--</li>--}}
{{--<li>--}}
{{--<a href="javascript: void(0)" class="cl-box amber" data-theme="colors/amber"></a>--}}
{{--</li>--}}
{{--<li>--}}
{{--<a href="javascript: void(0)" class="cl-box green-pantone" data-theme="colors/green-pantone"></a>--}}
{{--</li>--}}
{{--<li>--}}
{{--<a href="javascript: void(0)" class="cl-box orange-pantone" data-theme="colors/orange-pantone"></a>--}}
{{--</li>--}}
{{--<li>--}}
{{--<a href="javascript: void(0)" class="cl-box burnt-orange" data-theme="colors/burnt-orange"></a>--}}
{{--</li>--}}
{{--<li>--}}
{{--<a href="javascript: void(0)" class="cl-box blueberry" data-theme="colors/blueberry"></a>--}}
{{--</li>--}}
{{--<li>--}}
{{--<a href="javascript: void(0)" class="cl-box magenta-violet" data-theme="colors/magenta-violet"></a>--}}
{{--</li>--}}
{{--<li>--}}
{{--<a href="javascript: void(0)" class="cl-box blue-green" data-theme="colors/blue-green"></a>--}}
{{--</li>--}}
{{--<li>--}}
{{--<a href="javascript: void(0)" class="cl-box blue-munsell" data-theme="colors/blue-munsell"></a>--}}
{{--</li>--}}
{{--<li>--}}
{{--<a href="javascript: void(0)" class="cl-box coquelicot" data-theme="colors/coquelicot"></a>--}}
{{--</li>--}}
{{--</ul>--}}
{{--</div>--}}
<!-- /Switcher -->
    <a id="back2Top" class="theme-bg" title="Back to top" href="#"><i class="ti-arrow-up"></i></a>


    <!-- START JAVASCRIPT -->
    <script src="{{ asset('assets/js/jquery.min.js') }}"></script>
    <script src="{{ asset('assets/plugins/js/bootstrap.min.js') }}"></script>
    <script src="{{ asset('assets/plugins/js/bootsnav.js') }}"></script>
    <script src="{{asset('assets/plugins/js/bootstrap-select.min.js')}}"></script>
    <script src="{{ asset('assets/plugins/js/bootstrap-touch-slider-min.js') }}"></script>
    <script src="{{ asset('assets/plugins/js/jquery.touchSwipe.min.js') }}"></script>
    <script src="{{ asset('assets/plugins/js/chosen.jquery.js') }}"></script>
    <script src="{{ asset('assets/plugins/js/datedropper.min.js') }}"></script>
    <script src="{{ asset('assets/plugins/js/dropzone.js') }}"></script>
    <script src="{{ asset('assets/plugins/js/jquery.counterup.min.js') }}"></script>
    <script src="{{ asset('assets/plugins/js/jquery.fancybox.js') }}"></script>
    <script src="{{ asset('assets/plugins/js/jquery.nice-select.js') }}"></script>
    <script src="{{ asset('assets/plugins/js/jqueryadd-count.js') }}"></script>
    <script src="{{ asset('assets/plugins/js/jquery-rating.js') }}"></script>
    <script src="{{ asset('assets/plugins/js/slick.js') }}"></script>
    <script src="{{ asset('assets/plugins/js/timedropper.js') }}"></script>
    <script src="{{ asset('assets/plugins/js/waypoints.min.js') }}"></script>

    <script src="{{asset('assets/js/jQuery.style.switcher.js')}}"></script>

    <!-- Custom Js -->
    <script src="{{ asset('assets/js/custom.js') }}"></script>

    <script>
        function openRightMenu() {
            document.getElementById("rightMenu").style.display = "block";
        }

        function closeRightMenu() {
            document.getElementById("rightMenu").style.display = "none";
        }
    </script>

    <script type="text/javascript">
        $(document).ready(function () {
            $('#styleOptions').styleSwitcher();
        });
    </script>

    <script>
        $(document).ready(function () {
            $('[data-toggle="tooltip"]').tooltip();
        });
    </script>

    <script type="text/javascript">
        $(document).ready(function () {
            $('select').niceSelect();
        });
    </script>

</div>
</body>
</html>

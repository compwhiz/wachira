@extends('layouts.backend')

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-body">
                    <h4 class="card-title">Materials</h4>

                    <materials :allmaterials="{{ $Material }}"></materials>
                </div>
            </div>
        </div>
    </div>
@endsection

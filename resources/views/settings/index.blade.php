@extends('layouts.backend')

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="col-lg-12 grid-margin stretch-card">
                <div class="card">
                    <div class="card-body">
                        <h4 class="card-title">Settings</h4>
                        <small>Choose what to display</small>
                        <div class="col-lg-12">
                            <settings></settings>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

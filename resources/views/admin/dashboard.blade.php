@extends('layouts.backend')

@section('content')
    <div class="row">
        <div class="card">
            <div class="card-body">
                <h1 class="font-weight-light mb-4">{{ \App\Material::all()->count() }}</h1>
                <div class="d-flex flex-wrap align-items-center">
                    <div>
                        <h4 class="font-weight-normal">Materials</h4>
                        <p class="text-muted mb-0 font-weight-light">All Materials in Database</p>
                    </div>
                    <i class="mdi mdi-account-multiple icon-lg text-primary ml-auto"></i>
                </div>
            </div>
        </div>
    </div>
    {{--<div class="row">--}}
    {{--<div class="col-md-12">--}}
    {{--<div class="col-lg-12 grid-margin stretch-card">--}}
    {{--<div class="card">--}}
    {{--<div class="card-body">--}}
    {{--<h4 class="card-title">Add Materials</h4>--}}
    {{--<div class="table-responsive">--}}

    {{--<addmaterial></addmaterial>--}}

    {{--</div>--}}
    {{--</div>--}}
    {{--</div>--}}
    {{--</div>--}}
    {{--</div>--}}
    {{--</div>--}}
    <div class="row mt-5">
        <div class="col-md-12">
            <div class="card">
                <div class="card-body">
                    <h4 class="card-title">Materials</h4>

                    <materials :allmaterials='@json($Material)'></materials>
                </div>
            </div>
        </div>
    </div>
@endsection

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMaterialsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('materials', function (Blueprint $table) {
            $table->increments('id');
            $table->string('material');
            $table->string('category')->nullable();
            $table->string('composition')->nullable();
            $table->string('appearance')->nullable();
            $table->string('structure')->nullable();
            $table->string('meltingpoint')->nullable();
            $table->string('boilingpoint')->nullable();
            $table->string('density')->nullable();
            $table->string('shearmodulus')->nullable();
            $table->string('youngsmodulus')->nullable();
            $table->string('bulkmodulus')->nullable();
            $table->string('image')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('materials');
    }
}
